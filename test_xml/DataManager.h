//
//  DataManager.h
//  test_xml
//
//  Created by Михаил Рахмалевич on 24.07.12.
//  Copyright (c) 2012 _My Company Name_. All rights reserved.
//

#import <Foundation/Foundation.h>

@class Candidate;
@interface DataManager : NSObject <NSXMLParserDelegate> {
@private
    Candidate *_currentCandidate;
}

@property (nonatomic, retain, readonly) NSManagedObjectContext *managedObjectContext;
@property (nonatomic, retain, readonly) NSManagedObjectModel *managedObjectModel;
@property (nonatomic, retain, readonly) NSPersistentStoreCoordinator *persistentStoreCoordinator;

+ (DataManager *)sharedInstance;
- (void)loadCandidates;
- (NSFetchedResultsController *)candidatesController;

@end
