//
//  Experience.h
//  test_xml
//
//  Created by Михаил Рахмалевич on 24.07.12.
//  Copyright (c) 2012 _My Company Name_. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class Candidate;

@interface Experience : NSManagedObject

@property (nonatomic, retain) NSString * field;
@property (nonatomic, retain) NSNumber * level;
@property (nonatomic, retain) Candidate *candidate;

@end
