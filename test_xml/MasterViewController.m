//
//  MasterViewController.m
//  test_xml
//
//  Created by Михаил Рахмалевич on 24.07.12.
//  Copyright (c) 2012 _My Company Name_. All rights reserved.
//

#import "MasterViewController.h"
#import "DetailViewController.h"

@interface MasterViewController () 
@property (nonatomic, retain) NSFetchedResultsController *candidatesController;
@property (nonatomic, retain) NSArray *filteredModel;
- (void)setupModel;
- (void)searchByFragment:(NSString *)fragment;
@end

@implementation MasterViewController

@synthesize detailViewController = _detailViewController;
@synthesize busyView = _busyView, busyViewPlate = _busyViewPlate;
@synthesize tableView = _tableView;
@synthesize filteredModel = _filteredModel;
@synthesize candidatesController = _candidatesController;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        self.title = @"Candidates";
    }
    return self;
}
							
- (void)dealloc
{
    [_detailViewController release];
    [_candidatesController release];
    [_filteredModel release];
    [_busyView release];
    [_busyViewPlate release];
    [_tableView release];
    [super dealloc];
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return (interfaceOrientation != UIInterfaceOrientationPortraitUpsideDown);
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    _busyViewPlate.layer.cornerRadius = 5.0f;
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    if (!_candidatesController) {
        [self performSelector:@selector(setupModel) withObject:nil afterDelay:1.0];
    }
}

- (void)setupModel
{
    self.view.userInteractionEnabled = NO; _busyView.hidden = NO;
    [[DataManager sharedInstance] loadCandidates];    
    self.candidatesController = [[DataManager sharedInstance] candidatesController];
    [_tableView reloadData];
    self.view.userInteractionEnabled = YES; _busyView.hidden = YES;
}

- (void)searchByFragment:(NSString *)fragment
{
    self.filteredModel = [[_candidatesController fetchedObjects] filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"name contains[cd] %@ OR surname contains[cd] %@", fragment, fragment]];
}

#pragma mark - Table View

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return [tableView isEqual:_tableView] ? [[_candidatesController sections] count] : 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [tableView isEqual:_tableView] ? [[[_candidatesController sections] objectAtIndex:section] numberOfObjects] : [_filteredModel count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"Cell";
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {
        cell = [[[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier] autorelease];
        cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
    }

    Candidate *candidate = [tableView isEqual:_tableView] ? [_candidatesController objectAtIndexPath:indexPath] : [_filteredModel objectAtIndex:indexPath.row];
    cell.textLabel.text = [NSString stringWithFormat:@"%@ %@", candidate.name, candidate.surname];
    return cell;
}

- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section
{
    return [tableView isEqual:_tableView] ? [[[_candidatesController sections] objectAtIndex:section] name] : nil;
}

- (NSArray *)sectionIndexTitlesForTableView:(UITableView *)tableView
{
    NSArray *sectionIndex;
    if ([tableView isEqual:_tableView]) {
        NSMutableArray *sectionIndexMutable = [NSMutableArray arrayWithArray:[[_candidatesController sections] valueForKey:@"name"]];
        [sectionIndexMutable insertObject:@"{search}" atIndex:0];
        sectionIndex = [NSArray arrayWithArray:sectionIndexMutable];
    } else {
        sectionIndex = nil;
    }
    return sectionIndex;
}

- (NSInteger)tableView:(UITableView *)tableView sectionForSectionIndexTitle:(NSString *)title atIndex:(NSInteger)index
{
    if (index == 0) {
        [tableView scrollRectToVisible:CGRectMake(0, 0, 1, 1) animated:NO];
        return -1;
    } else {
        return (index - 1);
    }
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    if (!self.detailViewController) {
        self.detailViewController = [[[DetailViewController alloc] initWithNibName:@"DetailViewController" bundle:nil] autorelease];
    }
    
    Candidate *candidate = [tableView isEqual:_tableView] ? [_candidatesController objectAtIndexPath:indexPath] : [_filteredModel objectAtIndex:indexPath.row];
    self.detailViewController.detailItem = candidate;
    [self.navigationController pushViewController:self.detailViewController animated:YES];
}

#pragma mark - UISearchDisplayController delegate
- (BOOL)searchDisplayController:(UISearchDisplayController *)controller shouldReloadTableForSearchString:(NSString *)searchString
{
    [self searchByFragment:searchString];
    return YES;
}

@end
